/*

1. What built-in JS keyword is used to import packages?
	require()

2. What Node.js module/package contains a method for server creation?
	http module/package

3. What is the method of the http object responsible for creating a server using Node.js?
	createServer((request,response)=> {
	
			if (req.url === "/login") {
				res.writeHead(200, {"Content-Type" : "text/plain"});
				res.end("Please Log-In");
			}
			}else{
				res.writeHead(404, {"Content-Type" : "text/plain"});
				res.end("404: Page not found");
			}
	})

4. Between server and client, which creates a request?
	client

5. Between server and client, which creates a response?
	server

6. What is a runtime environment used to create stand-alone backend applications written in Javascript?
	node.js

7.What is the largest registry for Node packages?
	npm

*/